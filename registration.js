
function registration() {
    var name = document.getElementById("username").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    

    if (name === '' || email === '' || password === '') {
        alert('Please fill in all fields.');
    } else {
        alert('Form submitted successfully!');
        document.getElementById("registrationForm").reset();
        window.location.href='signin.html'
    }
   
}
function validateForm() {
    const email = document.getElementById('email').value;
    const emailError = document.getElementById('emailError');

    if (email.indexOf('@') === -1) {
        emailError.textContent = 'Email must contain the @ symbol.';
        return false;
    } else {
        emailError.textContent = '';
        return true;
    }
}

function checkPasswordStrength() {
    const password = document.getElementById('password').value;
    const strengthIndicator = document.getElementById('strengthIndicator');

    let strength = 0;

    if (/^[a-zA-Z]+$/.test(password)) {
        strength = 1;
    }

    if (/^[a-zA-Z0-9]+$/.test(password)) {
        strength = 2;
    }

    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(password)) {
        strength = 3;
    }

    displayStrength(strength);
}

function displayStrength(strength) {
    const strengthIndicator = document.getElementById('strengthIndicator');

    switch (strength) {
        case 1:
            strengthIndicator.textContent = 'Password Strength: Weak (Contains only letters of the English alphabet)';
            break;
        case 2:
            strengthIndicator.textContent = 'Password Strength: Medium (Contains letters and numbers of the English alphabet)';
            break;
        case 3:
            strengthIndicator.textContent = 'Password Strength: Strong (Contains letters of the English alphabet, uppercase and lowercase, and numbers)';
            break;
        default:
            strengthIndicator.textContent = '';
    }
}

